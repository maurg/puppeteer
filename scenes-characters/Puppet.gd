extends Node2D


const ATTACK_DISTANCE = 200

export var direction = 1
export var detection_distance = 1000
export var default_attack_damage = 10
export var attack_speed = 2
export var max_health = 20
export var floor_distance_keeping = 100
var attack_damage = default_attack_damage
var health = max_health

var movement = Vector2()
var start_attack_time = OS.get_ticks_msec()
var connected = false

const Dt = 200
var last_process = OS.get_ticks_msec()
export var tiles_mult = 2
export var control_mult = 0.1

signal floor_detected
signal enemy_detected

var target_y
var max_count
var count = 0

func _input(event):
	if(Input.is_action_pressed("ui_up")):
		$Holder/Holder2.target_y += 16 * tiles_mult
		print("target y: ", $Holder/Holder2.target_y)
	elif(Input.is_action_pressed("ui_down")):
		$Holder/Holder2.target_y -= 16 * tiles_mult
		print("target y: ", $Holder/Holder2.target_y)
	elif(Input.is_action_pressed("ui_right")):
		$Holder/Holder2.target_x += 16 * tiles_mult
		print("target x: ", $Holder/Holder2.target_x)
	elif(Input.is_action_pressed("ui_left")):
		$Holder/Holder2.target_x -= 16 * tiles_mult
		print("target x: ", $Holder/Holder2.target_x)
	elif(Input.is_action_pressed("ui_select")):
		try_attack()
		

func _ready():
	$FloorDetector.cast_to = Vector2(0, floor_distance_keeping*2)
	
	$FrontDetector.cast_to.x = detection_distance * direction
	$FrontDetector.cast_to.y = 0
	
	target_x = $Holder.position.x# + 200 * direction
	target_y = $Holder.position.y
	max_count = 1000 / Dt
	
	build_string($Holder/Holder2, $Body/Arm/LowerArm)


func _physics_process(delta):
	$Holder.set_angular_velocity(0)
	
	if(OS.get_ticks_msec() > last_process + Dt):
		$Holder.set_linear_velocity(Vector2(x_impulse(), y_impulse()))
		last_process = OS.get_ticks_msec()
		
		count += 1
		if(count >= max_count):
			print("X -> target:",target_x," actual:",$Holder.position.x," error:", target_x - $Holder.position.x)
			print("Y -> target:",target_y," actual:",$Holder.position.y," error:", target_y - $Holder.position.y)
			count = 0

	try_attack()

var ROPE_PIECE = preload("res://String.tscn")

func build_string(from, to):
	
	var start_point = from.get_global_transform().get_origin() 
	var end_point = to.get_node('AttachmentPoint').get_global_transform().get_origin()
	var piece_size = 16
	var local_end_point =  end_point - start_point
	var distance = sqrt(pow(local_end_point.x, 2) + pow(local_end_point.y,2))
	var angle = -asin(local_end_point.x / distance)
	var number_of_pieces = floor(distance / piece_size)
	from.set_rotation(angle)
	
	var parent = from
	for i in range(0, number_of_pieces):
		parent = add_piece(parent)
	
	var joint = parent.get_node("Joint")
	joint.node_a = parent.get_path()
	joint.node_b = to.get_path()
	
func add_piece(parent, child = ROPE_PIECE.instance()):
	var joint = parent.get_node("Joint")
	joint.add_child(child)
	joint.node_a = parent.get_path()
	joint.node_b = child.get_path()
	return child

#PI implementation to keep height in a natural manner
var y_integral = 0
const YKP = 1
const YKI = 0.1

func y_impulse():
	var error = target_y - $Holder.position.y
	y_integral = y_integral + error
	return YKP * error + YKI * y_integral

var target_x
var x_integral = 0
const XKP = 1
const XKI = 0

func x_impulse():
	var error = target_x - $Holder.position.x
	x_integral = x_integral + error
	return XKP * error + XKI * x_integral

func distance_to_enemy():	
	if($FrontDetector.is_colliding()):
		return abs($FrontDetector.get_collision_point().x - get_global_transform().get_origin().x)
	return null
	
func distance_to_floor():
	if($FloorDetector.is_colliding()):
		return abs($FloorDetector.get_collision_point().y - $FloorDetector.get_global_transform().get_origin().y)
	return null

func try_attack():
	var distance = distance_to_enemy()
	if (distance && distance < ATTACK_DISTANCE):
		if (elapsed_time() > attack_speed * 1000):
			attack()
	else:
		idle()

func elapsed_time():
	return (OS.get_ticks_msec() - start_attack_time)

func attack():
	start_attack_time = OS.get_ticks_msec()
	print(get_name(), " attack")
	#$AnimatedSprite.set_animation("attack")
	print(get_enemy())
	#attack_enemy(get_enemy())
		
func attack_enemy(node):
	if(node.has_method("attacked")):
		node.attacked(attack_damage)	
	else:
		attack_enemy(node.get_parent())

	
func idle():
	#$AnimatedSprite.set_animation("default")
	pass
	
func get_enemy():
	if($FrontDetector.is_colliding()):
		return $FrontDetector.get_collider()
	return null

func attacked(var damage):
	print(get_name(), " attacked for ", damage)
	health = max(health - damage, 0)
	if(health == 0):
		get_parent().remove_child(self)
		
func sunrise_handler():
	attack_damage = default_attack_damage / 2
	health = min(health, max_health/2)
	#set_scale(Vector2(0.7, 0.7))
	#melting animation
	
func sunset_handler():
	attack_damage = default_attack_damage
