extends "res://scenes_characters/Puppet.gd"

func _ready():
	set_modulate("4eacff")
	direction = -1
	health = 50
	add_to_group("monsters")
	MessageBus.connect("sunrise", self, "sunrise_handler")
	MessageBus.connect("sunset", self, "sunset_handler")
	init_ray_cast()

func sunrise_handler():
	attack_damage = default_attack_damage / 2
	health = min(health, max_health/2)
	#set_scale(Vector2(0.7, 0.7))
	#melting animation
	
func sunset_handler():
	attack_damage = default_attack_damage
