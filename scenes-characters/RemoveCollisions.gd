extends RigidBody2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	for node in get_parent().get_parent().get_children():
		if(node is RigidBody2D):
			add_collision_exception_with(node)



#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
