extends RayCast2D

func _ready():
	add_exception_to_tree(get_parent())
	
func add_exception_to_tree(node):
	if(node is RigidBody2D):
		add_exception(node)
	for child_node in node.get_children():
		add_exception_to_tree(child_node)
