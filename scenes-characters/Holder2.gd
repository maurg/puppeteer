extends RigidBody2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var last_process = 0
const Dt = 100
var initial_x

func _ready():
	target_y = position.y
	target_x = position.x

func _physics_process(delta):
	set_linear_velocity(Vector2(x_impulse(), y_impulse()))
	set_angular_velocity(rotation_impulse())
	#if(OS.get_ticks_msec() > last_process + Dt):
	#	apply_impulse(Vector2(), Vector2(x_impulse(), y_impulse()))
	#	last_process = OS.get_ticks_msec()

#PI implementation to keep height in a natural manner
var target_y
var y_integral = 0
const YKP = 100
const YKI = 0

func y_impulse():
	var error =  target_y - position.y
	y_integral = y_integral + error
	return YKP * error + YKI * y_integral

var target_x
var x_integral = 0
const XKP = 100
const XKI = 0

func x_impulse():
	var error = target_x - position.x
	x_integral = x_integral + error
	return XKP * error + XKI * x_integral
	
var target_rot = 0
var rot_integral = 0
const RKP = 0.5
const RKI = 0.005
	
func rotation_impulse():
	var error =  target_rot - rotation_degrees
	rot_integral = rot_integral + error
	return RKP * error + RKI * rot_integral
