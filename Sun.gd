extends Node2D

signal sunrise
signal sunset

func _ready():
	MessageBus.emit_signal("sunrise")

func _exit_tree():
	MessageBus.emit_signal("sunset")