extends Node

func _ready():
	var ice_monster_scene = preload("res://scenes-characters/Puppet.tscn")
	var ice_monster = ice_monster_scene.instance()
	ice_monster.set_position(Vector2(1216, 96))
	ice_monster.direction = -1
	ice_monster.health = 50
	ice_monster.add_to_group("monsters")
	add_child(ice_monster)
	
	MessageBus.connect("sunrise", ice_monster, "sunrise_handler")
	MessageBus.connect("sunset", ice_monster, "sunset_handler")
	
	#ice_monster.connect("enemy_detected", self, "enemy_detection")

var sun

func _on_SunButton_toggled(button_pressed):
	if(button_pressed):
		var sunScene = preload("res://Sun.tscn")
		sun = sunScene.instance()
		add_child(sun)
	else:
		self.remove_child(sun)
		sun = null